import { Aluno } from './../models/Aluno';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlunoService {

  constructor(private http: HttpClient) { }

  baseUrl = `${environment.baseUrl}/api/aluno`

  getAll(): Observable<Aluno[]> {
    return this.http.get<Aluno[]>(`${this.baseUrl}`)
  }

  getById(id: Number): Observable<Aluno> {
    return this.http.get<Aluno>(`${this.baseUrl}/${id}`)
  }

  post(aluno: Aluno) {
    return this.http.post(`${this.baseUrl}`, aluno)
  }

  put(aluno: Aluno) {
    return this.http.put(`${this.baseUrl}/${aluno.id}`, aluno)
  }

  delete(id: Number) {
    return this.http.delete(`${this.baseUrl}/${id}`)
  }
}
