import { AlunoService } from './aluno.service';
import { Aluno } from './../models/Aluno';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-alunos',
  templateUrl: './alunos.component.html',
  styleUrls: ['./alunos.component.css'],
})
export class AlunosComponent implements OnInit {
  public titulo = 'alunos';
  public alunoSeleciona: Aluno;
  public textSimple: string;
  public alunoForm: FormGroup;
  modalRef: BsModalRef;
  public modo = "post";
  public alunos: Aluno[];
  public loadAlunos: boolean;

  constructor(private fb: FormBuilder, private modalService: BsModalService, private alunoService: AlunoService) {
    this.criarForm();
  }

  ngOnInit(): void {
    this.loadAlunos = true;
    this.carregarAlunos();
  }

  carregarAlunos() {
    this.alunoService.getAll().subscribe(
      (alunos: Aluno[]) => { // caso sucesso
        this.alunos = alunos
        this.loadAlunos = false
      },
      (erro: any) => { // caso  error
        console.error(erro)
      }
    )
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  criarForm() {
    this.alunoForm = this.fb.group({
      id: [''],
      nome: ['', Validators.required],
      sobreNome: ['', Validators.required],
      telefone: ['', Validators.required],
    });
  }

  alunoSelecionado(aluno: Aluno) {
    this.alunoSeleciona = aluno;
    this.alunoForm.patchValue(aluno);
  }

  voltar() {
    this.alunoSeleciona = null;
  }

  salvarAluno(aluno: Aluno) {
    aluno.id === 0 ? this.modo : this.modo = "put"

    this.alunoService[this.modo](aluno).subscribe(
      (retorno: Aluno) => {
        console.log(retorno);
        this.carregarAlunos()
      },
      (err) => {
        console.error(err);
      }
    )
  }

  alunoSubmit() {
    this.salvarAluno(this.alunoForm.value);
  }

  alunoNovo() {
    this.alunoSeleciona = new Aluno();
    this.alunoForm.patchValue(this.alunoSeleciona);
  }

  deletarAluno(id: number) {
    this.alunoService.delete(id).subscribe(
      (model: any) => {
        console.log(model);
        this.carregarAlunos();

      },
      (erro: any) => {
        console.error(erro);

      }
    );
  }
}
