import { PerfisComponent } from './perfis/perfis.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import { AlunosComponent } from './alunos/alunos.component';
import { ProfessoresComponent } from './professores/professores.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'dashbord', pathMatch: 'full' },
  { path: 'dashbord', component: DashbordComponent },
  { path: 'perfis', component: PerfisComponent },
  { path: 'professores', component: ProfessoresComponent },
  { path: 'alunos', component: AlunosComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
