import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Professor } from '../models/Professor';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class ProfessorService {

  constructor(private http: HttpClient) { }

  baseUrl = `${environment.baseUrl}/api/professor`

  getAll(): Observable<Professor[]> {
    return this.http.get<Professor[]>(`${this.baseUrl}`)
  }

  getById(id: Number): Observable<Professor> {
    return this.http.get<Professor>(`${this.baseUrl}/${id}`)
  }

  post(professor: Professor) {
    return this.http.post(`${this.baseUrl}`, professor)
  }

  put(professor: Professor) {
    return this.http.put(`${this.baseUrl}/${professor.id}`, professor)
  }

  delete(id: Number) {
    return this.http.delete(`${this.baseUrl}/${id}`)
  }

}
