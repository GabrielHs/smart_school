import { ProfessorService } from './professor.service';
import { Professor } from './../models/Professor';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-professores',
  templateUrl: './professores.component.html',
  styleUrls: ['./professores.component.css'],
})
export class ProfessoresComponent implements OnInit {
  titulo = 'Professores';

  public professorSeleciona: Professor;
  public professorForm: FormGroup;
  modalRef: BsModalRef;
  public professores: Professor[];
  public modo = "post";
  public loadProfs: boolean;

  constructor(private fb: FormBuilder, private modalService: BsModalService, private professorService: ProfessorService) {
    this.criarForm();
  }

  ngOnInit(): void {

    this.loadProfs = true;
    this.carregarDado()
  }

  carregarDado() {
    this.professorService.getAll().subscribe(
      (profRetorno: Professor[]) => { // qnd sucesso
        this.professores = profRetorno
        this.loadProfs = false
      },
      (err: any) => {
        console.error(err);

      }
    );
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  profSelecionado(professor: Professor) {
    this.professorSeleciona = professor;
    this.professorForm.patchValue(professor)
  }

  professorNovo() {
    this.professorSeleciona = new Professor();
    this.professorForm.patchValue(this.professorSeleciona)
  }

  criarForm() {
    this.professorForm = this.fb.group({
      id: [''],
      nome: ['', Validators.required],
      // disciplina: ['', Validators.required],

    });
  }

  voltar() {
    this.professorSeleciona = null;
  }

  profSubmit() {
    // console.log(this.professorForm.value);
    this.salvarProfessor(this.professorForm.value)
  }

  salvarProfessor(professor: Professor) {
    professor.id === 0 ? this.modo : this.modo = "put";
    this.professorService[this.modo]( professor).subscribe(
      (corpoObjProfessor: Professor) => {
        console.log(corpoObjProfessor);
        this.carregarDado()
      },
      (err) => {
        console.error(err);
      }
    );
  }

}
